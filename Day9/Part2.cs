using Base;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day9
{
    public class Part2 : Solver<uint>
    {
        public override uint Solve(IList<string> input)
        {
            var match = Regex.Match(input[0],
                "(?<MaxPlayers>[0-9]+) players; last marble is worth (?<MaxPoints>[0-9]+) points");

            var maxPlayers = uint.Parse(match.Groups["MaxPlayers"].Value);
            var maxPoints = uint.Parse(match.Groups["MaxPoints"].Value) * 100;
            var scoreByPlayer = new Dictionary<uint, uint>();

            var circle = new LinkedList<uint>();
            var currentMarble = circle.AddLast(0u);
            var currentPlayer = 0u;
            var currentPoint = 0u;

            while (++currentPoint <= maxPoints)
            {
                if (++currentPlayer > maxPlayers)
                    currentPlayer = 1;

                if (currentPoint % 23 == 0)
                {
                    var previousNode = CounterClockwiseNode(currentMarble, 7);
                    AddScore(scoreByPlayer, currentPlayer, currentPoint + previousNode.Value);
                    currentMarble = ClockwiseNode(previousNode, 1);
                    circle.Remove(previousNode);
                }
                else
                {
                    var nextNode = ClockwiseNode(currentMarble, 1);
                    currentMarble = circle.AddAfter(nextNode, currentPoint);
                }
            }

            return scoreByPlayer.Values.Max();
        }

        private static LinkedListNode<uint> ClockwiseNode(LinkedListNode<uint> node, uint steps)
        {
            for (var i = 0u; i < steps; i++)
                node = node.Next ?? node.List.First;

            return node;
        }

        private static LinkedListNode<uint> CounterClockwiseNode(LinkedListNode<uint> node, uint steps)
        {
            for (var i = 0u; i < steps; i++)
                node = node.Previous ?? node.List.Last;

            return node;
        }

        private static void AddScore(IDictionary<uint, uint> scoreByPlayer, uint player, uint score)
        {
            if (!scoreByPlayer.ContainsKey(player))
                scoreByPlayer[player] = 0;

            scoreByPlayer[player] += score;
        }
    }
}