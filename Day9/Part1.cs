﻿using Base;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day9
{
    public class Part1 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var match = Regex.Match(input[0],
                "(?<MaxPlayers>[0-9]+) players; last marble is worth (?<MaxPoints>[0-9]+) points");

            var maxPlayers = int.Parse(match.Groups["MaxPlayers"].Value);
            var maxPoints = int.Parse(match.Groups["MaxPoints"].Value);
            var scoreByPlayer = new Dictionary<int, int>();

            var circle = new LinkedList<int>();
            var currentMarble = circle.AddLast(0);
            var currentPlayer = 0;
            var currentPoint = 0;

            while (++currentPoint <= maxPoints)
            {
                if (++currentPlayer > maxPlayers)
                    currentPlayer = 1;

                if (currentPoint % 23 == 0)
                {
                    var previousNode = CounterClockwiseNode(currentMarble, 7);
                    AddScore(scoreByPlayer, currentPlayer, currentPoint + previousNode.Value);
                    currentMarble = ClockwiseNode(previousNode, 1);
                    circle.Remove(previousNode);
                }
                else
                {
                    var nextNode = ClockwiseNode(currentMarble, 1);
                    currentMarble = circle.AddAfter(nextNode, currentPoint);
                }
            }

            return scoreByPlayer.Values.Max();
        }

        private static LinkedListNode<int> ClockwiseNode(LinkedListNode<int> node, int steps)
        {
            for (var i = 0; i < steps; i++)
                node = node.Next ?? node.List.First;

            return node;
        }

        private static LinkedListNode<int> CounterClockwiseNode(LinkedListNode<int> node, int steps)
        {
            for (var i = 0; i < steps; i++)
                node = node.Previous ?? node.List.Last;

            return node;
        }

        private static void AddScore(IDictionary<int, int> scoreByPlayer, int player, int score)
        {
            if (!scoreByPlayer.ContainsKey(player))
                scoreByPlayer[player] = 0;

            scoreByPlayer[player] += score;
        }
    }
}