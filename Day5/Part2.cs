using Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day5
{
    public class Part2 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var lengths = Enumerable.Range('a', 26)
                .AsParallel()
                .WithDegreeOfParallelism(Environment.ProcessorCount)
                .Select(c => ReactLength(input[0], (char) c));

            return lengths.Min();
        }

        private static int ReactLength(string input, char ignore)
        {
            var builder = new StringBuilder(input);

            builder.Replace($"{ignore}", string.Empty);
            builder.Replace($"{char.ToUpper(ignore)}", string.Empty);

            while (true)
            {
                var length = builder.Length;

                for (var c = 'a'; c <= 'z'; c++)
                {
                    if (c == ignore)
                        continue;

                    var firstReplace = $"{c}{char.ToUpper(c)}";
                    builder.Replace(firstReplace, string.Empty);

                    var secondReplace = $"{char.ToUpper(c)}{c}";
                    builder.Replace(secondReplace, string.Empty);
                }

                if (length == builder.Length)
                    break;
            }

            return builder.Length;
        }
    }
}