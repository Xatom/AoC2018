﻿using Base;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day5
{
    public class Part1 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var builder = new StringBuilder(input[0]);

            while (true)
            {
                var length = builder.Length;

                for (var c = 'a'; c <= 'z'; c++)
                {
                    var firstReplace = $"{c}{char.ToUpper(c)}";
                    builder.Replace(firstReplace, string.Empty);

                    var secondReplace = $"{char.ToUpper(c)}{c}";
                    builder.Replace(secondReplace, string.Empty);
                }

                if (length == builder.Length)
                    break;
            }

            return builder.Length;
        }
    }
}