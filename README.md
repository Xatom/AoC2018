# Advent of Code 2018

This repository contains all my solutions for the Advent of Code 2018 assignments.
I like doing these assignments because they have just the right amount of challenge.

Because I probably won't ever look at this code again and because I don't have a lot of time, the code is mostly undocumented and unoptimized.
However, I try to keep the execution time for every assignment less than one minute and all assignments produce valid output.

The assignments can be found [here](https://adventofcode.com/2018).
