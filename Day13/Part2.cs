using Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day13
{
    public class Part2 : Solver<(int x, int y)>
    {
        public override (int x, int y) Solve(IList<string> input)
        {
            var width = input.Max(i => i.Length);
            var height = input.Count;

            var rail = new char[width, height];
            var carts = new List<Cart>();

            ParseInput(input, rail, carts);

            while (carts.Count != 1)
                Tick(rail, carts);

            return (carts[0].X, carts[0].Y);
        }

        private static void ParseInput(IList<string> input, char[,] rail, ICollection<Cart> carts)
        {
            for (var x = 0; x < rail.GetLength(0); x++)
            for (var y = 0; y < rail.GetLength(1); y++)
            {
                var current = input[y][x];

                switch (current)
                {
                    case '<':
                    case '>':
                        rail[x, y] = '-';
                        carts.Add(new Cart(current, x, y));
                        break;

                    case '^':
                    case 'v':
                        rail[x, y] = '|';
                        carts.Add(new Cart(current, x, y));
                        break;

                    default:
                        rail[x, y] = input[y][x];
                        break;
                }
            }
        }

        private static void Tick(char[,] rail, ICollection<Cart> carts)
        {
            var orderedCarts = carts.OrderBy(c => c.Y).ThenBy(c => c.X);

            foreach (var cart in orderedCarts)
            {
                cart.Tick(rail[cart.X, cart.Y]);

                var collision = carts.FirstOrDefault(c => c != cart && c.X == cart.X && c.Y == cart.Y);
                if (collision == null)
                    continue;

                carts.Remove(cart);
                carts.Remove(collision);
            }
        }

        private class Cart
        {
            public Cart(char direction, int x, int y)
            {
                Direction = direction;
                X = x;
                Y = y;
            }

            private char Direction { get; set; }

            private int Intersection { get; set; }

            public int X { get; private set; }

            public int Y { get; private set; }

            private void TurnLeft()
            {
                switch (Direction)
                {
                    case '<':
                        Direction = 'v';
                        break;

                    case '>':
                        Direction = '^';
                        break;

                    case '^':
                        Direction = '<';
                        break;

                    case 'v':
                        Direction = '>';
                        break;

                    default:
                        throw new Exception("Current direction is invalid");
                }
            }

            private void TurnRight()
            {
                switch (Direction)
                {
                    case '<':
                        Direction = '^';
                        break;

                    case '>':
                        Direction = 'v';
                        break;

                    case '^':
                        Direction = '>';
                        break;

                    case 'v':
                        Direction = '<';
                        break;

                    default:
                        throw new Exception("Current direction is invalid");
                }
            }

            private void SetDirection(char rail)
            {
                switch (rail)
                {
                    case '|':
                    case '-':
                        break;

                    case '+' when Intersection == 0:
                        Intersection = 1;
                        TurnLeft();
                        break;

                    case '+' when Intersection == 1:
                        Intersection = 2;
                        break;

                    case '+' when Intersection == 2:
                        Intersection = 0;
                        TurnRight();
                        break;

                    case '/' when Direction == '<' || Direction == '>':
                    case '\\' when Direction == '^' || Direction == 'v':
                        TurnLeft();
                        break;

                    case '/' when Direction == '^' || Direction == 'v':
                    case '\\' when Direction == '<' || Direction == '>':
                        TurnRight();
                        break;

                    default:
                        throw new Exception("Current rail, intersection or direction is invalid");
                }
            }

            public void Tick(char rail)
            {
                SetDirection(rail);

                switch (Direction)
                {
                    case '<':
                        X--;
                        break;

                    case '>':
                        X++;
                        break;

                    case '^':
                        Y--;
                        break;

                    case 'v':
                        Y++;
                        break;

                    default:
                        throw new Exception("Current direction is invalid");
                }
            }
        }
    }
}