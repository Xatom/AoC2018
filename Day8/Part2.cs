using Base;
using System.Collections.Generic;
using System.Linq;

namespace Day8
{
    public class Part2 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var numbers = input[0].Split(' ').Select(int.Parse).ToList();

            var rootNode = new Node();
            FillNode(rootNode, numbers, 0);

            return NodeValue(rootNode);
        }

        private static int FillNode(Node node, IList<int> numbers, int position)
        {
            var childNodes = numbers[position++];
            var metadataEntries = numbers[position++];

            for (var childNode = 0; childNode < childNodes; childNode++)
            {
                var newNode = new Node();
                position = FillNode(newNode, numbers, position);
                node.Children.Add(newNode);
            }

            for (var metadataEntry = 0; metadataEntry < metadataEntries; metadataEntry++)
                node.Metadata.Add(numbers[position++]);

            return position;
        }

        private static int NodeValue(Node node)
        {
            if (node.Children.Count == 0)
                return node.Metadata.Sum();

            var value = 0;

            foreach (var metadata in node.Metadata)
            {
                var childIndex = metadata - 1;

                if (childIndex < 0 || childIndex >= node.Children.Count)
                    continue;

                value += NodeValue(node.Children[childIndex]);
            }

            return value;
        }

        private class Node
        {
            public List<int> Metadata { get; } = new List<int>();

            public List<Node> Children { get; } = new List<Node>();
        }
    }
}