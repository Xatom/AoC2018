﻿using Base;
using System.Collections.Generic;
using System.Linq;

namespace Day8
{
    public class Part1 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var numbers = input[0].Split(' ').Select(int.Parse).ToList();

            var rootNode = new Node();
            FillNode(rootNode, numbers, 0);

            return SumNode(rootNode);
        }

        private static int FillNode(Node node, IList<int> numbers, int position)
        {
            var childNodes = numbers[position++];
            var metadataEntries = numbers[position++];

            for (var childNode = 0; childNode < childNodes; childNode++)
            {
                var newNode = new Node();
                position = FillNode(newNode, numbers, position);
                node.Children.Add(newNode);
            }

            for (var metadataEntry = 0; metadataEntry < metadataEntries; metadataEntry++)
                node.Metadata.Add(numbers[position++]);

            return position;
        }

        private static int SumNode(Node node)
        {
            var sum = node.Metadata.Sum();

            foreach (var child in node.Children)
                sum += SumNode(child);

            return sum;
        }

        private class Node
        {
            public List<int> Metadata { get; } = new List<int>();

            public List<Node> Children { get; } = new List<Node>();
        }
    }
}