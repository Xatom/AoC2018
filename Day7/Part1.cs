﻿using Base;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day7
{
    public class Part1 : Solver<string>
    {
        public override string Solve(IList<string> input)
        {
            var rules = input.Select(i => new Rule(i))
                .ToList();

            var nodeByStep = new Dictionary<char, Node>();

            foreach (var rule in rules)
            {
                if (!nodeByStep.ContainsKey(rule.Step))
                    nodeByStep[rule.Step] = new Node(rule.Step);

                if (!nodeByStep.ContainsKey(rule.Dependency))
                    nodeByStep[rule.Dependency] = new Node(rule.Dependency);

                nodeByStep[rule.Step].Dependencies.Add(nodeByStep[rule.Dependency]);
            }

            var path = new StringBuilder();
            var completedSteps = new HashSet<char>();
            var nodes = nodeByStep.Values.ToList();

            ProcessChain(path, completedSteps, nodes);

            return path.ToString();
        }

        private static void ProcessChain(StringBuilder path, ISet<char> completedSteps, IList<Node> nodes)
        {
            var nodeToProcess = nodes.Where(n =>
                    !completedSteps.Contains(n.Step) &&
                    !n.Dependencies.Select(d => d.Step).Except(completedSteps).Any())
                .OrderBy(n => n.Step)
                .FirstOrDefault();

            if (nodeToProcess == null)
                return;

            path.Append(nodeToProcess.Step);
            completedSteps.Add(nodeToProcess.Step);

            ProcessChain(path, completedSteps, nodes);
        }

        private class Rule
        {
            public Rule(string line)
            {
                Step = line[36];
                Dependency = line[5];
            }

            public char Step { get; }

            public char Dependency { get; }
        }

        private class Node
        {
            public Node(char step)
            {
                Step = step;
            }

            public char Step { get; }

            public List<Node> Dependencies { get; } = new List<Node>();
        }
    }
}