using Base;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day7
{
    public class Part2 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var rules = input.Select(i => new Rule(i))
                .ToList();

            var nodeByStep = new Dictionary<char, Node>();

            foreach (var rule in rules)
            {
                if (!nodeByStep.ContainsKey(rule.Step))
                    nodeByStep[rule.Step] = new Node(rule.Step);

                if (!nodeByStep.ContainsKey(rule.Dependency))
                    nodeByStep[rule.Dependency] = new Node(rule.Dependency);

                nodeByStep[rule.Step].Dependencies.Add(nodeByStep[rule.Dependency]);
            }

            var schedule = new Scheduler();
            var nodes = nodeByStep.Values.ToList();

            ProcessChain(nodes, schedule);

            return schedule.Time;
        }

        private static void ProcessChain(IList<Node> nodes, Scheduler scheduler)
        {
            while (true)
            {
                var nodesToProcess = nodes.Where(scheduler.NodeCanBeProcessed)
                    .OrderBy(n => n.Step)
                    .Take(scheduler.AvailableWorkers)
                    .ToList();

                if (nodesToProcess.Count == 0)
                {
                    if (scheduler.Tick())
                        // There are currently no available nodes to process, but we were able to
                        // advance the time to a point where one or more workers have finished their jobs. 
                        continue;

                    // There are currently no available nodes to process and all workers are idle; we're done.
                    return;
                }

                foreach (var nodeToProcess in nodesToProcess)
                    scheduler.StartJob(nodeToProcess.Step);
            }
        }

        private class Scheduler
        {
            private class Job
            {
                public Job(char step, int currentTime)
                {
                    Step = step;
                    FinishedAt = currentTime + (step - 'A' + 1) + 60;
                }

                public char Step { get; }

                public int FinishedAt { get; }
            }

            private const int Workers = 5;

            private readonly StringBuilder path = new StringBuilder();

            private readonly List<Job> jobs = new List<Job>();

            private readonly HashSet<char> takenSteps = new HashSet<char>();

            private readonly HashSet<char> completedSteps = new HashSet<char>();

            public int AvailableWorkers => Workers - jobs.Count;

            public int Time { get; private set; }

            public void StartJob(char step)
            {
                takenSteps.Add(step);
                jobs.Add(new Job(step, Time));
            }

            public bool Tick()
            {
                if (jobs.Count == 0)
                    // We can't advance the time because there are no jobs.
                    return false;

                // Advance the time to the first completed job.
                Time = jobs.Min(j => j.FinishedAt);

                // Process all jobs that have been finished after advancing the time.
                foreach (var finishedJob in jobs.Where(j => j.FinishedAt <= Time).OrderBy(j => j.Step))
                {
                    completedSteps.Add(finishedJob.Step);
                    path.Append(finishedJob.Step);
                    jobs.Remove(finishedJob);
                }

                return true;
            }

            public bool NodeCanBeProcessed(Node node)
            {
                // A node can only be processed when it's not being processed already,
                // and all its dependencies have been resolved.
                return !takenSteps.Contains(node.Step) &&
                       !node.Dependencies.Select(d => d.Step).Except(completedSteps).Any();
            }
        }

        private class Rule
        {
            public Rule(string line)
            {
                Step = line[36];
                Dependency = line[5];
            }

            public char Step { get; }

            public char Dependency { get; }
        }

        private class Node
        {
            public Node(char step)
            {
                Step = step;
            }

            public char Step { get; }

            public List<Node> Dependencies { get; } = new List<Node>();
        }
    }
}