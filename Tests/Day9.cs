using Day9;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day9 : Base
    {
        public Day9(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<int, Part1>(9);
            Assert.Equal(424639, result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<uint, Part2>(9);
            Assert.Equal(3516007333u, result);
        }
    }
}