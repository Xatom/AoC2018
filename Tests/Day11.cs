using Day11;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day11 : Base
    {
        public Day11(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<(int x, int y), Part1>(11);
            Assert.Equal((243, 43), result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<(int x, int y, int size), Part2>(11);
            Assert.Equal((236, 151, 15), result);
        }
    }
}