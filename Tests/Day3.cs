using Day3;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day3 : Base
    {
        public Day3(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        void Part1()
        {
            var result = WithInput<int, Part1>(3);
            Assert.Equal(109716, result);
        }

        [Fact]
        void Part2()
        {
            var result = WithInput<int, Part2>(3);
            Assert.Equal(124, result);
        }
    }
}