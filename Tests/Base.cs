using Base;
using System;
using System.IO;
using Xunit.Abstractions;

namespace Tests
{
    public class Base
    {
        private readonly ITestOutputHelper output;

        protected Base(ITestOutputHelper output)
        {
            this.output = output;
        }

        protected T WithInput<T, TS>(int day) where TS : Solver<T>, new()
        {
            var solver = new TS();
            var input = File.ReadAllText($"input/day{day}");
            var lines = input.Split('\r', '\n');
            var result = solver.Solve(lines);

            output.WriteLine($"Result:{Environment.NewLine}{result}");

            return result;
        }
    }
}