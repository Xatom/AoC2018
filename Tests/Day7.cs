using Day7;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day7 : Base
    {
        public Day7(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<string, Part1>(7);
            Assert.Equal("FHICMRTXYDBOAJNPWQGVZUEKLS", result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<int, Part2>(7);
            Assert.Equal(946, result);
        }
    }
}