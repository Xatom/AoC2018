using Day6;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day6 : Base
    {
        public Day6(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<int, Part1>(6);
            Assert.Equal(3890, result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<int, Part2>(6);
            Assert.Equal(40284, result);
        }
    }
}