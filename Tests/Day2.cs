using Day2;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day2 : Base
    {
        public Day2(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<int, Part1>(2);
            Assert.Equal(6723, result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<string, Part2>(2);
            Assert.Equal("prtkqyluiusocwvaezjmhmfgx", result);
        }
    }
}