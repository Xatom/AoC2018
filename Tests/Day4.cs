using Day4;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day4 : Base
    {
        public Day4(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<int, Part1>(4);
            Assert.Equal(87681, result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<int, Part2>(4);
            Assert.Equal(136461, result);
        }
    }
}