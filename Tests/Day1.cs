using Day1;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day1 : Base
    {
        public Day1(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<int, Part1>(1);
            Assert.Equal(439, result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<int, Part2>(1);
            Assert.Equal(124645, result);
        }
    }
}