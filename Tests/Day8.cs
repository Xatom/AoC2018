using Day8;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day8 : Base
    {
        public Day8(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<int, Part1>(8);
            Assert.Equal(46829, result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<int, Part2>(8);
            Assert.Equal(37450, result);
        }
    }
}