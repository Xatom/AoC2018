using Day5;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day5 : Base
    {
        public Day5(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<int, Part1>(5);
            Assert.Equal(11720, result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<int, Part2>(5);
            Assert.Equal(4956, result);
        }
    }
}