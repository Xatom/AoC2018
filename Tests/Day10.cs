using Day10;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day10 : Base
    {
        public Day10(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<string, Part1>(10);
            Assert.Equal(" ####   #####      ###  #       #       #       #       #    #\n" +
                         "#    #  #    #      #   #       #       #       #       #    #\n" +
                         "#       #    #      #   #       #       #       #       #    #\n" +
                         "#       #    #      #   #       #       #       #       #    #\n" +
                         "#       #####       #   #       #       #       #       ######\n" +
                         "#  ###  #           #   #       #       #       #       #    #\n" +
                         "#    #  #           #   #       #       #       #       #    #\n" +
                         "#    #  #       #   #   #       #       #       #       #    #\n" +
                         "#   ##  #       #   #   #       #       #       #       #    #\n" +
                         " ### #  #        ###    ######  ######  ######  ######  #    #\n", result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<int, Part2>(10);
            Assert.Equal(10515, result);
        }
    }
}