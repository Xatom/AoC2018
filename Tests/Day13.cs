using Day13;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class Day13 : Base
    {
        public Day13(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void Part1()
        {
            var result = WithInput<(int x, int y), Part1>(13);
            Assert.Equal((130, 104), result);
        }

        [Fact]
        public void Part2()
        {
            var result = WithInput<(int x, int y), Part2>(13);
            Assert.Equal((29, 83), result);
        }
    }
}