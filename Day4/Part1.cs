﻿using Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day4
{
    public class Part1 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var schedules = input.Select(i => new Info(i))
                .OrderBy(i => i.Date);

            var minutesById = new Dictionary<int, List<int>>();
            var timeSpanById = new Dictionary<int, TimeSpan>();

            var maxId = default(int);
            var currentId = default(int);
            var currentStart = default(DateTime);

            foreach (var schedule in schedules)
            {
                switch (schedule.Action)
                {
                    case Action.Begin:
                        currentId = schedule.Id;
                        currentStart = schedule.Date;

                        if (!minutesById.ContainsKey(currentId))
                            minutesById.Add(currentId, new List<int>());

                        if (!timeSpanById.ContainsKey(currentId))
                            timeSpanById.Add(currentId, default);

                        break;
                    case Action.Asleep:
                        currentStart = schedule.Date;

                        break;

                    case Action.Awake:
                        var sleepTime = schedule.Date - currentStart;
                        timeSpanById[currentId] += sleepTime;

                        for (var i = currentStart.Minute; i < currentStart.Minute + sleepTime.Minutes; i++)
                            minutesById[currentId].Add(i);

                        break;
                }

                if (maxId == default || timeSpanById[currentId] > timeSpanById[maxId])
                    maxId = currentId;
            }

            var maxMinute = minutesById[maxId]
                .GroupBy(m => m)
                .OrderByDescending(m => m.Count())
                .First()
                .Key;

            return maxId * maxMinute;
        }

        private enum Action
        {
            Begin,
            Asleep,
            Awake
        }

        private class Info
        {
            private static readonly Regex IdRegex = new Regex("#(?<Id>[0-9]+)", RegexOptions.Compiled);

            public Info(string line)
            {
                var date = line.Substring(1, 16);
                var action = line.Substring(19);

                switch (action)
                {
                    case "falls asleep":
                        Action = Action.Asleep;
                        break;

                    case "wakes up":
                        Action = Action.Awake;
                        break;

                    default:
                        Id = int.Parse(IdRegex.Match(action).Groups["Id"].Value);
                        Action = Action.Begin;
                        break;
                }

                Date = DateTime.ParseExact(date, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
            }

            public int Id { get; }

            public Action Action { get; }

            public DateTime Date { get; }
        }
    }
}