using Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day4
{
    public class Part2 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var schedules = input.Select(i => new Info(i))
                .OrderBy(i => i.Date);

            var minutesById = new Dictionary<int, List<int>>();

            var currentId = default(int);
            var currentStart = default(DateTime);

            foreach (var schedule in schedules)
            {
                switch (schedule.Action)
                {
                    case Action.Begin:
                        currentId = schedule.Id;
                        currentStart = schedule.Date;

                        if (!minutesById.ContainsKey(currentId))
                            minutesById.Add(currentId, new List<int>());

                        break;
                    case Action.Asleep:
                        currentStart = schedule.Date;

                        break;

                    case Action.Awake:
                        var sleepTime = schedule.Date - currentStart;

                        for (var i = currentStart.Minute; i < currentStart.Minute + sleepTime.Minutes; i++)
                            minutesById[currentId].Add(i);

                        break;
                }
            }

            var maxId = default(int);
            var maxCount = default(int);
            var maxMinute = default(int);

            foreach (var minuteById in minutesById.Where(m => m.Value.Count > 0))
            {
                var entry = minuteById.Value
                    .GroupBy(m => m, (key, values) => new
                    {
                        Minute = key,
                        Count = values.Count()
                    })
                    .OrderByDescending(m => m.Count)
                    .First();

                if (entry.Count <= maxCount)
                    continue;

                maxId = minuteById.Key;
                maxCount = entry.Count;
                maxMinute = entry.Minute;
            }

            return maxId * maxMinute;
        }

        private enum Action
        {
            Begin,
            Asleep,
            Awake
        }

        private class Info
        {
            private static readonly Regex IdRegex = new Regex("#(?<Id>[0-9]+)", RegexOptions.Compiled);

            public Info(string line)
            {
                var date = line.Substring(1, 16);
                var action = line.Substring(19);

                switch (action)
                {
                    case "falls asleep":
                        Action = Action.Asleep;
                        break;

                    case "wakes up":
                        Action = Action.Awake;
                        break;

                    default:
                        Id = int.Parse(IdRegex.Match(action).Groups["Id"].Value);
                        Action = Action.Begin;
                        break;
                }

                Date = DateTime.ParseExact(date, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
            }

            public int Id { get; }

            public Action Action { get; }

            public DateTime Date { get; }
        }
    }
}