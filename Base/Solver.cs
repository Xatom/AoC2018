﻿using System.Collections.Generic;

namespace Base
{
    public abstract class Solver<T>
    {
        public abstract T Solve(IList<string> input);
    }
}