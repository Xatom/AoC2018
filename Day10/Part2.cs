using Base;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day10
{
    public class Part2 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var coordinates = input.Select(i => new Coordinate(i))
                .ToList();

            var sizeByIteration = new Dictionary<int, (int minX, int maxX, int minY, int maxY)>();

            for (var i = 0; i < 20_000; i++)
            {
                var minX = coordinates.Min(c => c.X + i * c.XVelocity);
                var maxX = coordinates.Max(c => c.X + i * c.XVelocity);
                var minY = coordinates.Min(c => c.Y + i * c.YVelocity);
                var maxY = coordinates.Max(c => c.Y + i * c.YVelocity);

                sizeByIteration[i] = (minX, maxX, minY, maxY);
            }

            return sizeByIteration
                .OrderBy(c => c.Value.maxX - c.Value.minX + c.Value.maxY - c.Value.minY)
                .First()
                .Key;
        }

        private class Coordinate
        {
            private static readonly Regex Regex = new Regex(
                "position=< ?(?<X>-?[0-9]+),  ?(?<Y>-?[0-9]+)> velocity=< ?(?<XV>-?[0-9]+),  ?(?<YV>-?[0-9]+)>",
                RegexOptions.Compiled);

            public Coordinate(string line)
            {
                var match = Regex.Match(line);

                X = int.Parse(match.Groups["X"].Value);
                Y = int.Parse(match.Groups["Y"].Value);
                XVelocity = int.Parse(match.Groups["XV"].Value);
                YVelocity = int.Parse(match.Groups["YV"].Value);
            }

            public int X { get; }

            public int Y { get; }

            public int XVelocity { get; }

            public int YVelocity { get; }
        }
    }
}