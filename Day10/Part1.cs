﻿using Base;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Day10
{
    public class Part1 : Solver<string>
    {
        public override string Solve(IList<string> input)
        {
            var coordinates = input.Select(i => new Coordinate(i))
                .ToList();

            var sizeByIteration = new Dictionary<int, (int minX, int maxX, int minY, int maxY)>();

            for (var i = 0; i < 20_000; i++)
            {
                var minX = coordinates.Min(c => c.X + i * c.XVelocity);
                var maxX = coordinates.Max(c => c.X + i * c.XVelocity);
                var minY = coordinates.Min(c => c.Y + i * c.YVelocity);
                var maxY = coordinates.Max(c => c.Y + i * c.YVelocity);

                sizeByIteration[i] = (minX, maxX, minY, maxY);
            }

            var targetIteration = sizeByIteration
                .OrderBy(c => c.Value.maxX - c.Value.minX + c.Value.maxY - c.Value.minY)
                .First()
                .Key;

            var width = sizeByIteration[targetIteration].maxX - sizeByIteration[targetIteration].minX + 1;
            var height = sizeByIteration[targetIteration].maxY - sizeByIteration[targetIteration].minY + 1;
            var grid = new bool[width, height];

            foreach (var coordinate in coordinates)
            {
                var x = (coordinate.X + targetIteration * coordinate.XVelocity) - sizeByIteration[targetIteration].minX;
                var y = (coordinate.Y + targetIteration * coordinate.YVelocity) - sizeByIteration[targetIteration].minY;

                if (x < 0 || y < 0 || x >= width || y >= height)
                    continue;

                grid[x, y] = true;
            }

            return GridToString(grid);
        }

        private static string GridToString(bool[,] grid)
        {
            var builder = new StringBuilder();

            for (var y = 0; y < grid.GetLength(1); y++)
            {
                for (var x = 0; x < grid.GetLength(0); x++)
                    builder.Append(grid[x, y] ? '#' : ' ');

                builder.AppendLine();
            }

            return builder.ToString();
        }

        private class Coordinate
        {
            private static readonly Regex Regex = new Regex(
                "position=< ?(?<X>-?[0-9]+),  ?(?<Y>-?[0-9]+)> velocity=< ?(?<XV>-?[0-9]+),  ?(?<YV>-?[0-9]+)>",
                RegexOptions.Compiled);

            public Coordinate(string line)
            {
                var match = Regex.Match(line);

                X = int.Parse(match.Groups["X"].Value);
                Y = int.Parse(match.Groups["Y"].Value);
                XVelocity = int.Parse(match.Groups["XV"].Value);
                YVelocity = int.Parse(match.Groups["YV"].Value);
            }

            public int X { get; }

            public int Y { get; }

            public int XVelocity { get; }

            public int YVelocity { get; }
        }
    }
}