using Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day6
{
    public class Part2 : Solver<int>
    {
        private const int MaximumDistance = 10_000;

        public override int Solve(IList<string> input)
        {
            var coordinates = input.Select(i => new Coordinate(i))
                .ToList();

            var validCells = 0;
            var width = coordinates.Max(c => c.X) + 1;
            var height = coordinates.Max(c => c.Y) + 1;

            for (var x = 0; x < width; x++)
            for (var y = 0; y < height; y++)
            {
                var totalDistance = 0;

                foreach (var coordinate in coordinates)
                    totalDistance += ManhattanDistance(x, y, coordinate.X, coordinate.Y);

                if (totalDistance < MaximumDistance)
                    validCells++;
            }

            return validCells;
        }

        /// <summary>
        /// Calculate the Manhattan distance between (X1, Y1) and (X2, Y2).
        /// </summary>
        private static int ManhattanDistance(int x, int y, int originalX, int originalY) =>
            Math.Abs(x - originalX) + Math.Abs(y - originalY);

        private class Coordinate
        {
            private static readonly Regex CoordinateRegex =
                new Regex("(?<X>[0-9]+), (?<Y>[0-9]+)", RegexOptions.Compiled);

            public Coordinate(string line)
            {
                var match = CoordinateRegex.Match(line);

                X = int.Parse(match.Groups["X"].Value);
                Y = int.Parse(match.Groups["Y"].Value);
            }

            public int X { get; }

            public int Y { get; }
        }
    }
}