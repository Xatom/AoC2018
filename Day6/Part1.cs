﻿using Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day6
{
    public class Part1 : Solver<int>
    {
        private const int EqualDistanceId = -1;

        public override int Solve(IList<string> input)
        {
            // Create a grid that's big enough to fit all coordinates.
            var grid = new Grid(input);

            // Draw all coordinates on the board first.
            foreach (var coordinate in grid.Coordinates)
                grid.Value[coordinate.X, coordinate.Y] = new Cell(coordinate.Id, 0);

            // Expand the area of every coordinate, one layer at a time.
            // This allows checking for collisions on every update.
            var maxDistance = Math.Max(grid.Width, grid.Height) - 1;
            for (var distance = 1; distance < maxDistance; distance++)
                foreach (var coordinate in grid.Coordinates)
                    grid.Expand(coordinate.Id, coordinate.X, coordinate.Y, distance);

            var infiniteIds = new HashSet<int>();
            var sizeById = grid.Coordinates.ToDictionary(c => c.Id, c => 0);

            // Mark all IDs touching the corners of the grid as infinite.
            foreach (var coordinate in GetSquare(0, 0, grid.Width - 1, grid.Height - 1))
            {
                var distance = grid.Value[coordinate.x, coordinate.y];

                if (distance != null)
                    infiniteIds.Add(distance.Id);
            }

            // Count all areas that are not infinite.
            for (var x = 0; x < grid.Width; x++)
            for (var y = 0; y < grid.Height; y++)
            {
                var distance = grid.Value[x, y];

                if (distance == null || distance.Id == EqualDistanceId || infiniteIds.Contains(distance.Id))
                    continue;

                sizeById[distance.Id]++;
            }

            return sizeById.Values.Max();
        }

        /// <summary>
        /// Calculate the Manhattan distance between (X1, Y1) and (X2, Y2).
        /// </summary>
        private static int ManhattanDistance(int x, int y, int originalX, int originalY) =>
            Math.Abs(x - originalX) + Math.Abs(y - originalY);

        /// <summary>
        /// Return the coordinates between (XLeft, YTop, XRight, YBottom):
        /// +----YT----+
        /// |          |
        /// XL        XR
        /// |          |
        /// +----YB----+
        /// </summary>
        /// <remarks>Corners are returned separately, because otherwise the loops will return them twice.</remarks>
        private static IEnumerable<(int x, int y)> GetSquare(int leftX, int topY, int rightX, int bottomY)
        {
            yield return (leftX, topY);
            yield return (rightX, topY);

            for (var currentX = leftX + 1; currentX <= rightX - 1; currentX++)
            {
                yield return (currentX, topY);
                yield return (currentX, bottomY);
            }

            yield return (leftX, bottomY);
            yield return (rightX, bottomY);

            for (var currentY = topY + 1; currentY <= bottomY - 1; currentY++)
            {
                yield return (leftX, currentY);
                yield return (rightX, currentY);
            }
        }

        private class Coordinate
        {
            private static readonly Regex CoordinateRegex =
                new Regex("(?<X>[0-9]+), (?<Y>[0-9]+)", RegexOptions.Compiled);

            private static int counter;

            public Coordinate(string line)
            {
                var match = CoordinateRegex.Match(line);

                Id = counter++;
                X = int.Parse(match.Groups["X"].Value);
                Y = int.Parse(match.Groups["Y"].Value);
            }

            public int Id { get; }

            public int X { get; }

            public int Y { get; }
        }

        private class Cell
        {
            public Cell(int id, int distance)
            {
                Id = id;
                Distance = distance;
            }

            public int Id { get; set; }

            public int Distance { get; }
        }

        private class Grid
        {
            public Grid(IEnumerable<string> input)
            {
                Coordinates = input.Select(i => new Coordinate(i))
                    .ToList();

                Width = Coordinates.Max(c => c.X) + 1;
                Height = Coordinates.Max(c => c.Y) + 1;
                Value = new Cell[Width, Height];
            }

            public int Width { get; }

            public int Height { get; }

            public Cell[,] Value { get; }

            public List<Coordinate> Coordinates { get; }

            private HashSet<int> FullyExpandedIds { get; } = new HashSet<int>();

            /// <summary>
            /// Try to expand the area of (X, Y) at the given distance.
            /// </summary>
            public void Expand(int id, int x, int y, int distance)
            {
                if (FullyExpandedIds.Contains(id))
                    return;

                var leftX = x - distance;
                var topY = y - distance;
                var rightX = leftX + distance * 2;
                var bottomY = topY + distance * 2;

                // If we can't fill any more surrounding cells now, we can't do it on a larger distance either.
                var hasUpdate = GetSquare(leftX, topY, rightX, bottomY).Aggregate(false,
                    (current, coordinate) => current | Fill(id, coordinate.x, coordinate.y, x, y));

                if (!hasUpdate)
                    FullyExpandedIds.Add(id);
            }

            /// <summary>
            /// Fill a single cell of the area, if allowed.  
            /// </summary>
            private bool Fill(int id, int x, int y, int xBase, int yBase)
            {
                // We need a neighbour cell to prevent filling unconnected cells.
                if (x < 0 || y < 0 || x >= Width || y >= Height || !HasNeighbour(id, x, y))
                    return false;

                var current = Value[x, y];
                var distance = ManhattanDistance(x, y, xBase, yBase);

                if (current != null && current.Id != id)
                {
                    if (current.Distance < distance)
                        // Cell is already closer to another coordinate.
                        return false;

                    if (current.Distance == distance)
                    {
                        // Another coordinate is at the same distance, we can't use it.
                        current.Id = EqualDistanceId;
                        return false;
                    }
                }

                Value[x, y] = new Cell(id, distance);
                return true;
            }

            private bool HasNeighbour(int id, int x, int y)
            {
                foreach (var coordinate in GetSquare(x - 1, y - 1, x + 1, y + 1))
                {
                    if (coordinate.x < 0 || coordinate.y < 0 || coordinate.x >= Width || coordinate.y >= Height)
                        continue;

                    if (Value[coordinate.x, coordinate.y]?.Id == id)
                        return true;
                }

                return false;
            }
        }
    }
}