using Base;
using System;
using System.Collections.Generic;

namespace Day2
{
    public class Part2 : Solver<string>
    {
        public override string Solve(IList<string> input)
        {
            foreach (var line in input)
            {
                foreach (var otherLine in input)
                {
                    if (CompareLines(line, otherLine, out var index))
                        return line.Remove(index, 1);
                }
            }

            throw new Exception("Unable to find matching lines");
        }

        private static bool CompareLines(string lineA, string lineB, out int index)
        {
            index = -1;

            for (var i = 0; i < lineA.Length; i++)
            {
                if (lineA[i] == lineB[i])
                    continue;

                if (index != -1)
                    // There can only be exactly one difference.
                    return false;

                index = i;
            }

            return index != -1;
        }
    }
}