﻿using Base;
using System.Collections.Generic;

namespace Day2
{
    public class Part1 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var checksumTwo = 0;
            var checksumThree = 0;

            foreach (var line in input)
            {
                var counter = new Dictionary<char, int>();

                foreach (var character in line)
                    counter[character] = counter.GetValueOrDefault(character) + 1;

                if (counter.ContainsValue(2))
                    checksumTwo++;

                if (counter.ContainsValue(3))
                    checksumThree++;
            }

            return checksumTwo * checksumThree;
        }
    }
}