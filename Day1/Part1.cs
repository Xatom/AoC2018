﻿using Base;
using System.Collections.Generic;
using System.Linq;

namespace Day1
{
    public class Part1 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            return input.Sum(int.Parse);
        }
    }
}