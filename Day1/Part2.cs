using Base;
using System.Collections.Generic;

namespace Day1
{
    public class Part2 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var frequency = 0;
            var pastFrequencies = new HashSet<int>();

            while (true)
            {
                foreach (var line in input)
                {
                    frequency += int.Parse(line);

                    if (!pastFrequencies.Add(frequency))
                        return frequency;
                }

                // Start again from the beginning until a match is found.
            }
        }
    }
}