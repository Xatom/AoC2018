﻿using Base;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Day11
{
    public class Part1 : Solver<(int x, int y)>
    {
        private const int Width = 300;
        private const int Height = 300;
        private const int MatchSize = 3;

        public override (int x, int y) Solve(IList<string> input)
        {
            var serialNumber = int.Parse(input[0]);
            var fuelCells = new int[Width, Height];

            for (var x = 0; x < Width; x++)
            for (var y = 0; y < Height; y++)
                fuelCells[x, y] = CalculatePowerLevel(x, y, serialNumber);

            var highestX = 0;
            var highestY = 0;
            var highestPower = int.MinValue;

            for (var cellX = 0; cellX <= Width - MatchSize; cellX++)
            for (var cellY = 0; cellY <= Height - MatchSize; cellY++)
            {
                var currentPower = 0;

                for (var squareX = cellX; squareX < cellX + MatchSize; squareX++)
                for (var squareY = cellY; squareY < cellY + MatchSize; squareY++)
                    currentPower += fuelCells[squareX, squareY];

                if (currentPower < highestPower)
                    continue;

                highestX = cellX;
                highestY = cellY;
                highestPower = currentPower;
            }

            return (highestX, highestY);
        }

        private static int CalculatePowerLevel(int x, int y, int serialNumber)
        {
            var rackId = x + 10;
            var powerLevel = rackId * y;

            powerLevel += serialNumber;
            powerLevel *= rackId;
            powerLevel %= 1000;
            powerLevel /= 100;
            powerLevel -= 5;

            return powerLevel;
        }
    }
}