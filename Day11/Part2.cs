using System;
using Base;
using System.Collections.Generic;
using System.Linq;

namespace Day11
{
    public class Part2 : Solver<(int x, int y, int size)>
    {
        private const int Width = 300;
        private const int Height = 300;

        public override (int x, int y, int size) Solve(IList<string> input)
        {
            var serialNumber = int.Parse(input[0]);
            var fuelCells = new int[Width, Height];

            for (var x = 0; x < Width; x++)
            for (var y = 0; y < Height; y++)
                fuelCells[x, y] = CalculatePowerLevel(x, y, serialNumber);

            var max = Enumerable.Range(1, 300)
                .AsParallel()
                .WithDegreeOfParallelism(Environment.ProcessorCount)
                .Select(s => new
                {
                    Size = s,
                    Data = GetHighestPower(fuelCells, s)
                }).OrderByDescending(s => s.Data.power)
                .First();

            return (max.Data.x, max.Data.y, max.Size);
        }

        private static (int x, int y, int power) GetHighestPower(int[,] fuelCells, int size)
        {
            var highestX = 0;
            var highestY = 0;
            var highestPower = int.MinValue;

            for (var cellX = 0; cellX <= Width - size; cellX++)
            for (var cellY = 0; cellY <= Height - size; cellY++)
            {
                var currentPower = 0;

                for (var squareX = cellX; squareX < cellX + size; squareX++)
                for (var squareY = cellY; squareY < cellY + size; squareY++)
                    currentPower += fuelCells[squareX, squareY];

                if (currentPower < highestPower)
                    continue;

                highestX = cellX;
                highestY = cellY;
                highestPower = currentPower;
            }

            return (highestX, highestY, highestPower);
        }

        private static int CalculatePowerLevel(int x, int y, int serialNumber)
        {
            var rackId = x + 10;
            var powerLevel = rackId * y;

            powerLevel += serialNumber;
            powerLevel *= rackId;
            powerLevel %= 1000;
            powerLevel /= 100;
            powerLevel -= 5;

            return powerLevel;
        }
    }
}