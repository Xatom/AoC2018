﻿using Base;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Day3
{
    public class Part1 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var fabric = new int[1000, 1000];
            var duplicateCount = 0;

            foreach (var line in input)
            {
                var info = new Info(line);

                for (var x = info.X; x < info.X + info.Width; x++)
                for (var y = info.Y; y < info.Y + info.Height; y++)
                {
                    if (fabric[x, y] == 1)
                        duplicateCount++;

                    fabric[x, y]++;
                }
            }

            return duplicateCount;
        }

        private class Info
        {
            private static readonly Regex Regex =
                new Regex("#[0-9]+ @ (?<X>[0-9]+),(?<Y>[0-9]+): (?<Width>[0-9]+)x(?<Height>[0-9]+)",
                    RegexOptions.Compiled);

            public Info(string line)
            {
                var match = Regex.Match(line);

                X = int.Parse(match.Groups["X"].Value);
                Y = int.Parse(match.Groups["Y"].Value);
                Width = int.Parse(match.Groups["Width"].Value);
                Height = int.Parse(match.Groups["Height"].Value);
            }

            public int X { get; }

            public int Y { get; }

            public int Width { get; }

            public int Height { get; }
        }
    }
}