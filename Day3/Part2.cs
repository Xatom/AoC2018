using Base;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day3
{
    public class Part2 : Solver<int>
    {
        public override int Solve(IList<string> input)
        {
            var allInfos = new List<Info>();
            var fabric = new Info[1000, 1000];

            foreach (var line in input)
            {
                var info = new Info(line);
                allInfos.Add(info);

                for (var x = info.X; x < info.X + info.Width; x++)
                for (var y = info.Y; y < info.Y + info.Height; y++)
                {
                    if (fabric[x, y] != null)
                    {
                        info.HasOverlap = true;
                        fabric[x, y].HasOverlap = true;
                    }

                    fabric[x, y] = info;
                }
            }

            return allInfos.First(i => i.HasOverlap == false).Id;
        }

        private class Info
        {
            private static readonly Regex Regex =
                new Regex("#(?<Id>[0-9]+) @ (?<X>[0-9]+),(?<Y>[0-9]+): (?<Width>[0-9]+)x(?<Height>[0-9]+)",
                    RegexOptions.Compiled);

            public Info(string line)
            {
                var match = Regex.Match(line);

                Id = int.Parse(match.Groups["Id"].Value);
                X = int.Parse(match.Groups["X"].Value);
                Y = int.Parse(match.Groups["Y"].Value);
                Width = int.Parse(match.Groups["Width"].Value);
                Height = int.Parse(match.Groups["Height"].Value);
            }

            public int Id { get; }

            public int X { get; }

            public int Y { get; }

            public int Width { get; }

            public int Height { get; }

            public bool HasOverlap { get; set; }
        }
    }
}